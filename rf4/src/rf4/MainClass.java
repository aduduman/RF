package rf4;

import rf2.FileUtils;

public class MainClass {
	
	
	public static void main(String[] args) {
		double[][] learningSet;
		try {
			learningSet = FileUtils.readLearningSetFromFile("in2.txt");
			int numberOfPatterns = learningSet.length;
			int numberOfFeatures = learningSet[0].length -1;
			System.out.println(String.format("The learning set has %s patterns and %s features", numberOfPatterns, numberOfFeatures));
			
			
//			//calculate Euclidian
//			double[] firstPattern = learningSet[0];
//			for(int i=1; i<numberOfPatterns; i++)
//			{
//				System.out.println("Distance between 1 and " + i + " pattern : " + Euclidian(firstPattern, learningSet[i]));
//				System.out.println("Cebisev between 1 and " + i + " pattern : " + Cebisev(firstPattern, learningSet[i]));
//				System.out.println("City Block between 1 and " + i + " pattern : " + CityBlock(firstPattern, learningSet[i]));
//				System.out.println("Mahalanobis between 1 and " + i + " pattern : " + Mahalanobis(firstPattern, learningSet[i], numberOfPatterns));
//			}
			FileUtils.writeLearningSetToFile("scaledSet.csv", EuclidianD(learningSet));
			
			System.out.println("Ultima linie apartine clasei " + NN(learningSet));
			
		} catch (USVInputFileCustomException e) {
			System.out.println(e.getMessage());
		} finally {
			System.out.println("Finished learning set operations");
		}
	}
	
	
	protected static double NN(double[][] learning_set)
	{
		double minim = Euclidian(learning_set[0], learning_set[learning_set.length -1 ]);
		int positie = 0;
		double temp;
		for(int i=0;i<learning_set.length;i++)
		{
			temp = Euclidian(learning_set[learning_set.length-1],learning_set[i]);
			if(temp<minim && temp!=0) { minim = temp; positie = i; }
		}
		return learning_set[positie][learning_set[positie].length-1];
	}
	
	
	
	
	
	protected static double[][] EuclidianD(double[][] learning_set)
	{
		double[][] output = new double[learning_set.length][];
		for(int i=0;i<learning_set.length;i++)
		{
			output[i] = new double[learning_set.length];
		}
		int k =0;
		for(int i=0;i<learning_set.length/2 + 2 ;i++)
		{
			for(int j=0;j<learning_set.length;j++)
			{
				output[i][j] = Math.floor(Euclidian(learning_set[i], learning_set[j])* 100)/ 100;
				output[j][i]= output[i][j] ;
				k++;
			}
		}
		//System.out.println(k+"");
		
		return output;
	}
	
	
	protected static double Euclidian(double[] pattern1, double[] pattern2)
	{
		double sum = 0D;
		for(int i=0;i<pattern1.length;i++)
		{
			sum+=(Math.pow(pattern1[i]-pattern2[i],2));
		}
		return Math.sqrt(sum);
		//return Math.sqrt(Math.pow(pattern1[0]-pattern2[0],2) + Math.pow(pattern1[1]- pattern2[1], 2));
	}
	
	
	protected static double Cebisev(double[] pattern1, double[] pattern2)
	{
		double[] minime = new double[pattern1.length];
		for(int i=0;i<pattern1.length;i++)
		{
			minime[i] = Math.abs(pattern1[i] - pattern2[i]);
		}
		
		double max = -9999;
		
		for(int i=0;i<minime.length;i++)
		{
			if(minime[i] > max)
				max = minime[i];
		}
		
		return max;
	}
	
	protected static double CityBlock(double[] pattern1, double[] pattern2)
	{
		double sum = 0;
		for(int i=0;i<pattern1.length;i++)
		{
			sum+= Math.abs(pattern1[i] - pattern2[i]);
		}
		
		return sum;
	}
	
	
	protected static double Mahalanobis(double[] pattern1, double[] pattern2, double nr)
	{
		double sum = 0;
		for(int i=0;i<pattern1.length;i++)
		{
			sum+= Math.pow(pattern1[i] - pattern2[i], nr);
		}
		return Math.pow(sum, 1/nr);
	}

}
