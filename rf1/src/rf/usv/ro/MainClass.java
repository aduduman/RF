package rf.usv.ro;


import java.awt.List;
import java.util.ArrayList;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;


public class MainClass extends ApplicationFrame {

	static double[] data;
	public static void main(String[] args) {
		double[][] learningSet = FileUtils.readLearningSetFromFile("in.txt");
		 FileUtils.writeLearningSetToFile("out.csv", normalizeLearningSet(learningSet));
		 
		data = new double[2 * learningSet.length ];
		int count = 0;
		for(int i=0;i<learningSet.length;i++)
		{
			data[count++] = learningSet[i][0];
			data[count++] = learningSet[i][1];
		}
	    System.out.println(learningSet.length);

	    final MainClass demo = new MainClass("Duduman Graph");
	    demo.pack();
	    RefineryUtilities.centerFrameOnScreen(demo);
	    demo.setVisible(true);

	}

	public MainClass(final String title) {

		   super(title);
		   final XYSeries series = new XYSeries("Hakuna Matata");
		   for(int i=0;i<data.length;i=i+2)
		   {
			   series.add(data[i], data[i+1]);
		   }
		  // series.add(1, 7);
		  // series.add(2, 3);
		  // series.add(1, 6);
		  // series.add(2, 5);
		  // series.add(1, 6);


		   final XYSeriesCollection data = new XYSeriesCollection(series);
		   final JFreeChart chart = ChartFactory.createXYLineChart(
		       "COME TO PAPA 0.5 POINTS BONUS !!",
		       "X", 
		       "Y", 
		       data,
		       PlotOrientation.VERTICAL,
		       true,
		       true,
		       false
		   );

		   final ChartPanel chartPanel = new ChartPanel(chart);
		   chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
		   setContentPane(chartPanel);
		   
	}
		   
	private static double[][] normalizeLearningSet(double[][] learningSet)
	{
		double[][] normalizedLearningSet = new double[learningSet.length][];
		ArrayList<Double> minList = new ArrayList<>();
		ArrayList<Double> maxList = new ArrayList<>();
		//.. enter your code here
		for(int i = 0;i<learningSet.length;i++) {
			normalizedLearningSet[i] = new double[learningSet[i].length];
			double min = 999999;
			double max = -99999;
			for(int j = 0; j<learningSet[i].length;j++) {
				if(min > learningSet[i][j]) {
					min = learningSet[i][j];
				}
				
				if(max < learningSet[i][j]) {
					max = learningSet[i][j];
				}
			}
			minList.add(min);
			maxList.add(max);
		}
		
		for(int i = 0;i<learningSet.length;i++) {
			for(int j = 0; j<learningSet[i].length;j++) {
				normalizedLearningSet[i][j] = (learningSet[i][j] - minList.get(j))/(maxList.get(j) - minList.get(j));
			}
		}
		return normalizedLearningSet;
	}

}