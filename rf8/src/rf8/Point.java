package rf8;

public class Point  /* implements Comparable<Point>*/{
		  double x,y; // bla bla x y
		  double[] distance; // distanta la nucleu
		  
		//public int compareTo(Point compareNeighbor) {
						
			//ascending order
			//return Double.compare(this.distance, compareNeighbor.distance);		
		//}
		
		public Point()
		{
			x = 0;
			y = 0;
			distance = new double[1];
		}
		
		public Point(double x, double y, int M)
		{
			this.x = x;
			this.y = y;
			distance = new double[M];
		}
		
		public Point(Point b)
		{
			this.x = b.x;
			this.y = b.y;
			this.distance = new double[b.distance.length];
			for(int i=0;i<this.distance.length;i++)
			{
				this.distance[i] = b.distance[i];
			}
		}
		
		public int minimumDistance()
		{
			double minim = Double.MAX_VALUE;
			int pozitie = 0;
			for(int i=0;i<distance.length;i++)
			{
				if (minim > distance[i] && distance[i] != 0)
					{minim = distance[i]; pozitie = i;}
			}
			return pozitie;
		}
}
