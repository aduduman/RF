package rf8;

import java.util.ArrayList;

public class MainClass {
	
	
	public static void main(String[] args) {
		double[][] learningSet;
		try {
			learningSet = FileUtils.readLearningSetFromFile("in.txt");
			int numberOfPatterns = learningSet.length;
			int numberOfFeatures = learningSet[0].length;
			System.out.println(String.format("The learning set has %s patterns and %s features", numberOfPatterns, numberOfFeatures));
			
			
			double[] iClass = new double[learningSet.length];
			iClass = FileUtils.dynamicKernels(learningSet, 2);
			
			for(int i=0;i<iClass.length;i++)
			{
				//System.out.println(iClass[i]);
			}
						
		} catch (USVInputFileCustomException e) {
			System.out.println(e.getMessage());
		} finally {
			System.out.println("Finished learning set operations");
		}
	}

}
