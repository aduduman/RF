package rf3;

import rf2.StatisticsUtils;

public class MainClass {
	
	
	public static void main(String[] args) {
		double[][] learningSet;
		try {
			learningSet = FileUtils.readLearningSetFromFile("in.txt");
			int numberOfPatterns = learningSet.length;
			int numberOfFeatures = learningSet[0].length;
			System.out.println(String.format("The learning set has %s patters and %s features", numberOfPatterns, numberOfFeatures));
			
			
			//calculate Euclidian
			double[] firstPattern = learningSet[0];
			for(int i=1; i<numberOfPatterns; i++)
			{
				System.out.println("Distance between 1 and " + i + " pattern : " + Euclidian(firstPattern, learningSet[i]));
				System.out.println("Cebisev between 1 and " + i + " pattern : " + Cebisev(firstPattern, learningSet[i]));
				System.out.println("City Block between 1 and " + i + " pattern : " + CityBlock(firstPattern, learningSet[i]));
				System.out.println("Mahalanobis between 1 and " + i + " pattern : " + Mahalanobis(firstPattern, learningSet[i], numberOfPatterns));
			}
			
			
		} catch (USVInputFileCustomException e) {
			System.out.println(e.getMessage());
		} finally {
			System.out.println("Finished learning set operations");
		}
	}
	
	
	protected static double Euclidian(double[] pattern1, double[] pattern2)
	{
		return Math.sqrt(Math.pow(pattern1[0]-pattern2[0],2) + Math.pow(pattern1[1]- pattern2[1], 2));
	}
	
	
	protected static double Cebisev(double[] pattern1, double[] pattern2)
	{
		double[] minime = new double[pattern1.length];
		for(int i=0;i<pattern1.length;i++)
		{
			minime[i] = Math.abs(pattern1[i] - pattern2[i]);
		}
		
		double max = -9999;
		
		for(int i=0;i<minime.length;i++)
		{
			if(minime[i] > max)
				max = minime[i];
		}
		
		return max;
	}
	
	protected static double CityBlock(double[] pattern1, double[] pattern2)
	{
		double sum = 0;
		for(int i=0;i<pattern1.length;i++)
		{
			sum+= Math.abs(pattern1[i] - pattern2[i]);
		}
		
		return sum;
	}
	
	
	protected static double Mahalanobis(double[] pattern1, double[] pattern2, double nr)
	{
		double sum = 0;
		for(int i=0;i<pattern1.length;i++)
		{
			sum+= Math.pow(pattern1[i] - pattern2[i], nr);
		}
		return Math.pow(sum, 1/nr);
	}

}
