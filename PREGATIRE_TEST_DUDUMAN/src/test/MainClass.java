package test;

public class MainClass {
	public static void main(String[] args)
	{
		try {
			
			
			
			double[][] learningSet = FileUtils.readLearningSetFromFile("in.txt");
			
			int numbersOfPatterns = learningSet.length;
			int numbersOfFutures = learningSet[0].length - 1;
			
			System.out.println("Patterns = " + numbersOfPatterns + " & Futures = " + numbersOfFutures);
			System.out.println("---------------------------------------------------------------------");

			for(int i=0;i<numbersOfPatterns;i++)
			{
				for(int j=0;j<numbersOfFutures;j++)
				{
					System.out.print(learningSet[i][j] + " ");
				}
				System.out.println();
			}
			System.out.println("---------------------------------------------------------------------");

			double[] weightFuture = new double[learningSet.length];
			for(int i=0;i<learningSet.length;i++)
				weightFuture[i] = learningSet[i][numbersOfFutures];
			
			for(int i=0;i<numbersOfFutures;i++)
			{
				double[] Future = new double[learningSet.length];
				for(int j=0;j<learningSet.length;j++)
				{
					Future[j] = learningSet[j][i];
				}
				System.out.println("Future no. " + (i+1) + " AVG = " + futureAverage(Future));
				System.out.println("Future no. " + (i+1) + " W-AVG = " + weightFutureAverage(Future, weightFuture));
				double weightFeature = weightFutureAverage(Future, weightFuture);
				System.out.println("Future no. " + (i+1) + " FeatureDispersion = " + featureDispersion(Future, weightFeature));
			}
			System.out.println("---------------------------------------------------------------------");
			
			int featureNumber = 3;
			double elementOccurrence = -2;
			double[] Future = new double[learningSet.length];
			for(int j=0;j<learningSet.length;j++)
			{
				Future[j] = learningSet[j][featureNumber-1];
			}
			System.out.println("Future no " + featureNumber + " has occurence = " + frequencyOccurrence(Future, elementOccurrence) + " for " + elementOccurrence);
			
			FileUtils.writeLearningSetToFile("out.csv", normalizeLearningSet(learningSet));
					
		} catch (Exception e) {
		// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static double featureDispersion(double[] feature, double weightedAverage)
	{
		double sum = 0;
		for(int i=0;i<feature.length;i++)
		{
			sum+=Math.pow(feature[i] - weightedAverage, 2);
		}
		return sum/(feature.length-1);
	}
	
	public static double frequencyOccurrence(double[] future, double element)
	{
		double count = 0;
		for(int i=0;i<future.length;i++)
		{
			if(future[i]==element) count++;
		}
		return count/future.length;
	}
	
	public static double weightFutureAverage(double[] future, double[] weight)
	{
		double sum1 = 0;
		double sum2 = 0;
		for(int i=0; i<future.length;i++)
		{
			sum1+=future[i]*weight[i];
			sum2+=weight[i];
		}
		return sum1/sum2;
	}
	
	public static double futureAverage(double[] Future)
	{
		double sum = 0;
		double count = Future.length;
		for(int i=0;i<Future.length;i++)
		{
			sum+=Future[i];
		}
		return sum/count;
	}
	
	public static double returnJMin(double[] pattern)
	{
		double min = 999999;
		for(int i=0;i<pattern.length-1;i++)
		{
			if(min>pattern[i])
				min = pattern[i];
		}
		return min;
	}
	
	public static double returnJMax(double[] pattern)
	{
		double max = -999999;
		for(int i=0;i<pattern.length-1;i++)
		{
			if(max<pattern[i])
				max = pattern[i];
		}
		return max;
	}
	
	public static double[][] normalizeLearningSet(double[][] learningSet)
	{
		double[][] normalizedLearningSet = new double[learningSet.length][learningSet[0].length];
		for(int i=0;i<learningSet.length;i++)
		{
			double max = returnJMax(learningSet[i]);
			double min = returnJMin(learningSet[i]);
						
			for(int j=0;j<learningSet[i].length;j++)
			{
				normalizedLearningSet[i][j] = (learningSet[i][j]-min)/(max-min);
			}
		}
		return normalizedLearningSet;
	}
}
